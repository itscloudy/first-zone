package keys;

public class Instance {

    public static void main(String[] args) {
        Parent parent = getParent();

        if (parent instanceof Child)
            ((Child) parent).hello();
    }

    static abstract class Parent {}
    static class Child extends Parent {

        void hello() {
            System.out.println("Hello");
        }
    }

    private static Parent getParent() {
        return new Child();
    }
}
