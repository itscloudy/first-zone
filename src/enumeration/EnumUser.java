package enumeration;

public class EnumUser {

    private void run() {
        Enumer e = Enumer.A;

        if (e.equals(Enumer.B)) {
            System.out.println("It's B");
        }else
            System.out.println("It's A");

        System.out.println(Enumer.A);
        System.out.println(Enumer.B);
    }

    public static void main(String args[]) {
        new EnumUser().run();
    }
}
