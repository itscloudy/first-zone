package generic;

import Inheritance.Ancient;
import Inheritance.Medieval;

class Zero<K extends Medieval, V extends Ancient, T> {

    private String incantation;

    Zero(String incantation) {
        this.incantation = incantation;
    }

    void activeTotem(K k, V v, T t) {

        k.describe();
        v.describe();

        System.out.println("INCANTATION : " + incantation + " ~> TOTEM : " + k.totem + " => TOTEM : " + v.totem );

        System.out.println("T's class : " + t.getClass().getSimpleName());
    }

    <X> void getXName(X x) {
        System.out.println(x.getClass().getName() + " : " + x.toString());
    }
}
