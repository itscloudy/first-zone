package generic;

import Inheritance.Medieval;
import Inheritance.Modern;

public class One {

    public static void main(String args[]) {

//        Zero<Modern, Medieval, ?> zero = new Zero<>("al");
        Zero zero = new Zero<>("al");

        zero.activeTotem(new Modern(), new Medieval(), "haha");

        zero.getXName("Hi, this is overwhelming");

        zero.getXName(100);
    }
}
