package numberType;

import java.math.BigDecimal;

public class FloatTest {

    private static BigDecimal bigDecimal = new BigDecimal("10.1");
    private static BigDecimal bigDecimal2 = new BigDecimal("10");

    public static void main(String[] args) {
        double f = 10.1f;
        double f2 = 100000001f;
        System.out.println(f - 10);
        System.out.println(f2);
        System.out.println(bigDecimal.subtract(bigDecimal2).toString());
    }
}
