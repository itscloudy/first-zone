package swing;


import javax.swing.*;

public class UIInfo {

    public static void main(String args[]) {
        UIDefaults uiDefaults = UIManager.getLookAndFeelDefaults();

        for(Object o: uiDefaults.keySet()) {
            if(o.toString().startsWith("Separator"))
            System.out.println(o + " " + uiDefaults.get(o));
        }

        for(UIManager.LookAndFeelInfo l: UIManager.getInstalledLookAndFeels()) {
            System.out.println(l.toString());
        }

        System.out.println(uiDefaults.get("Separator.background"));
    }
}
