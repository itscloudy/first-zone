package operator;

import java.util.Arrays;
import java.util.List;

public class DoubleColons {

    public static void main(String[] args) {

        List<String> list = Arrays.asList("a", "b");
        list.forEach(System.out::println);

        useConvert(DoubleColons::remaining);
        useConvert(DoubleColons::counting);

        // static methods
        IConvert<String, Integer> convert = Something::lengthCount;
        int len = convert.convert("123");
        System.out.println(len);

        // object methods
        Something something = new Something();
        IConvert<String, String> converter = something::endWith;
        String converted = converter.convert("Java");
        System.out.println(converted);

        // constructor methods
        IConvert<String, Something> convert2 = Something::new;
        Something something2 = convert2.convert("constructors");

        Transform<String, Transformer, Integer> transform = Transformer::new;
        Transformer transformer = transform.transform("Optimus prime", 120);
        System.out.println(transformer.toString());
    }

    private static String remaining(String time) {
        return time + "remaining";
    }

    private static String counting(String number) {
        return "now it have " + number;
    }

    private static void useConvert(IConvert<String, String> convert) {
        int i = 5;
        while (i > 0) {
            System.out.println(convert.convert(i + " seconds "));
            i--;
        }
    }
}

class Something {

    // constructor methods
    Something() {}

    Something(String something) {
        System.out.println(something);
    }

    // static methods
    static String startsWith(String s) {
        return String.valueOf(s.charAt(0));
    }

    static int lengthCount(String s) {
        return s.length();
    }

    // object methods
    String endWith(String s) {
        return String.valueOf(s.charAt(s.length()-1));
    }

    void endWith() {}
}

@FunctionalInterface
interface IConvert<F, T> {
    T convert(F form);
}

// -----
//@FunctionalInterface
interface Transform<A, B, C> {

    B transform(A a, C c);
}

class Transformer {

    private String name;
    private int high;

    Transformer(String name, int high) {
        this.name = name;
        this.high = high;
    }

    @Override
    public String toString() {
        return "This is a transformer name " + name + ", it has " + high + " feet";
    }
}