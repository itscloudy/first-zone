package operator;

public class LambdaTest {
    LambdaTestIF lambdaTestIF;

    void test() {
        lambdaTestIF = (a, b) -> {
            System.out.println(a + b);
            System.out.println(a - b);
        };
    }

    private static void get(LambdaTestGet get) {
        System.out.println(get.getString());
    }

    public static void main(String[] args) {
        String str = "Hello";
        get(()-> "em... " + str + " World");
    }
}

interface LambdaTestIF {

    public void sum(int a, int b);
}

interface LambdaTestGet {

    String getString();
}