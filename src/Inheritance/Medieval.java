package Inheritance;

public class Medieval extends Ancient {
    public String totem = "Dark Knight";

    /**
     * a method will still use the parent class fields if the used fields don't have 'this' prefix.
     */
    public void describe() {
        System.out.println("This's " + this.getClass().getSimpleName() + ", The Totem is " + totem);
    }
}
