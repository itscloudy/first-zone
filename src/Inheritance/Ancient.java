package Inheritance;

public class Ancient {

    /**
     * static fields will be static in the inherit line.
     */
    public String totem = "Ancient Relic";

    /**
     * inherited classes will invoke this one if they don't have a method override it.
     */
    public void describe() {
        System.out.println("This's " + this.getClass().getSimpleName() + ", The Totem is " + totem);
    }
}
