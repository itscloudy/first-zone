package string;

import java.util.StringJoiner;

public class StringJoinerTest {

    public static void main(String args[]) {
        StringJoiner joiner = new StringJoiner(" | ", "[ ", " ]");
        joiner.add("A");
        joiner.add("B");
        joiner.add("C");
        System.out.println(joiner.toString());
    }
}
