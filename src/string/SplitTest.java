package string;

public class SplitTest {

    private void test() {
        String s = "abc\ndef";

        String ss[] = s.split("\n");

        for(String str : ss) {
            System.out.println(str);
        }

    }

    public static void main(String args[]) {
        new SplitTest().test();
    }
}
