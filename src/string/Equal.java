package string;

import java.util.Objects;

public class Equal {
    public static void main(String[] args) {
        String a = getString(), b = getString();

        System.out.println(Objects.equals(a, b));

        EqualTest ea = new EqualTest("a", 1), eb = new EqualTest("a", 1);

        System.out.println(ea == eb);
    }

    static String getString() {
        int index = 500;
        StringBuilder s = new StringBuilder();
        while (index > 0) {
            s.append("a");
            index--;
        }
        return s.toString();
    }
}

class EqualTest {
    String a;
    int b;
    EqualTest(String a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EqualTest) {
            EqualTest compare = (EqualTest)obj;
            return compare.a.equals(this.a) && this.b == compare.b;
        }
        return false;
    }
}
