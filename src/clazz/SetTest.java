package clazz;

import java.util.HashSet;
import java.util.Set;

public class SetTest {

    public static void main(String[] args) {
        Set<ForTest> forTests = new HashSet<>();

        forTests.add(new ForTest(1, "a"));
        forTests.add(new ForTest(2, "a"));
        forTests.add(new ForTest(1, "a"));
        for (ForTest forTest: forTests) {
            System.out.println(forTest.toString());
        }

    }


    public static class ForTest {
        int a;
        String b;

        public ForTest(int a, String b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public String toString() {
            return a + " " + b;
        }
    }
}
