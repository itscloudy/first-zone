package clazz;

public class Class {

    int test() {
        L l = new L();
        return l.a;
    }
    public static void main(String args[]) {
        C c = new C();
        System.out.println(c.a);
        System.out.println(new Class().test());
    }
    public static class C {
        int a = 0;
    }

    public class L {
        int a = 0;
    }
}
