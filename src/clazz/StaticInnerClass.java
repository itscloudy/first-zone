package clazz;

public class StaticInnerClass {


    public static class In {

        public static class InIn {
            public void hello() {
                System.out.println("hello from static inner class");
            }
        }
    }

    public static void main(String[] args) {
        // 创建静态内部类
        new In.InIn().hello();
        InnerClass innerClass = new InnerClass();
        // 创建内部类
        InnerClass.In in = innerClass.new In();
        in.hello();
    }
}
