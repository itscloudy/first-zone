package clazz;

public class Type {

    public static void main(String[] args) {
        Type_A a = new Type_B();
        System.out.println(a.getClass());
    }
}

class Type_A {

    public String a () {
        return "A";
    }
}

class Type_B extends Type_A {

    public String b () {
        return "B";
    }
}
