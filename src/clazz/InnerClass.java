package clazz;

public class InnerClass {

    public class In {
        void hello() {
            System.out.println("hello from inner class");
        }
    }

    private In getIn() {
        return new In();
    }

}
