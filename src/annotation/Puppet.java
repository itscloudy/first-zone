package annotation;

@Annotation(realName = "Puppet")
public class Puppet {

    public String stageName;

    public Puppet(String stageName) {
        this.stageName = stageName;
        sayRealName();
    }

    private void sayRealName() {
        System.out.println(this.getClass().getAnnotation(Annotation.class).realName());
    }
}
